package it.forgiarini.learningjava.guava.predicates;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.base.Predicate;

public final class CustomPredicates {

	private CustomPredicates() {}
	
	public static Predicate<Number> evenNumbers() {
		return NumberPredicates.EVEN_NUMBERS;
	}
	
	public static Predicate<Number> oddNumbers() {
		return NumberPredicates.ODD_NUMBERS;
	}
	
	private enum NumberPredicates implements Predicate<Number> {
		EVEN_NUMBERS {

			@Override
			public boolean apply(@Nullable Number input) {
				if (input == null) {
					return false;
				}
				if (input.doubleValue() % 2 == 0) {
					return true;
				}
				return false;
			}

			@Override
			public String toString() {
				return "CustomPredicates.evenNumbers()";
			}

		},
		ODD_NUMBERS {
			
			@Override
			public boolean apply(@Nullable Number input) {
				if (input == null) {
					return false;
				}
				if (input.doubleValue() % 2 == 1) {
					return true;
				}
				return false;
			}
			
			@Override
			public String toString() {
				return "CustomPredicates.oddNumbers()";
			}
			
		},

	}
}
