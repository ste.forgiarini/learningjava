package it.forgiarini.learningjava.lombok;

import java.util.HashMap;

import lombok.val;
import lombok.var;

public class LombokMain {

	public static void main(String[] args) {
		
		// VAL FEATURE
		val finalMap = new HashMap<String, Integer>(); // The val keyword automatically infer the type. It is final. 
		finalMap.put("Rome", 4_000_000);
		
		// VAR FEATURE
		var notFinalMap = new HashMap<String, Integer>(); // The val keyword automatically infer the type. It is NOT final
		notFinalMap.put("Milan", 4_000_000);
		notFinalMap = null; // It is not final so we can change the reference of the map2 variable. 
	}
	
}
