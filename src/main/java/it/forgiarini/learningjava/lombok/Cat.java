package it.forgiarini.learningjava.lombok;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString(onlyExplicitlyIncluded = true)
public class Cat {

	@ToString.Include(name="NAME", rank=20)
	private final String name;
	@ToString.Include(name="AGE", rank=10)
	private final Integer age;
	
}
