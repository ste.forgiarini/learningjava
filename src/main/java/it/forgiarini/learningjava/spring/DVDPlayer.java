package it.forgiarini.learningjava.spring;

import org.springframework.stereotype.Component;

@Component
public class DVDPlayer implements MediaPlayer {

	@Override
	public void play() {
		System.out.println("Playing DVD");
	}

	@Override
	public void record() {
		System.out.println("Recording DVD");
	}

}
