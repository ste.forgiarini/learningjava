package it.forgiarini.learningjava.spring;

import org.springframework.stereotype.Component;

@Component
public class CDPlayer implements MediaPlayer {

	@Override
	public void play() {
		System.out.println("Playing CD");
	}

	@Override
	public void record() {
		System.out.println("Recording CD");
	}

}
