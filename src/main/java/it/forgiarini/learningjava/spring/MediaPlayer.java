package it.forgiarini.learningjava.spring;

public interface MediaPlayer {

	void play();
	
	void record();
	
}
