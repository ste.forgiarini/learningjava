package it.forgiarini.learningjava.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import it.forgiarini.learningjava.spring.CDPlayer;
import it.forgiarini.learningjava.spring.DVDPlayer;
import it.forgiarini.learningjava.spring.MediaPlayer;

@Configuration
public class SpringJavaConfig {

	@Bean
	@Primary
	public MediaPlayer dvdPlayer() {
		return new DVDPlayer();
	}
	
	@Bean("cdPlayer1")
	public MediaPlayer cdPlayer() {
		return new CDPlayer();
	}
	
}
