package it.forgiarini.learningjava.validation;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;

import it.forgiarini.learningjava.validation.beans.Address;
import it.forgiarini.learningjava.validation.beans.Person;

public class BaseValidationTest {

	protected Validator validator;
	private static final String NAME = "Stefano";
	private static final String SURNAME = "Forgiarini";
	private static final int AGE = 30;
	private static final String STREET = "Via Roma 12";
	private static final String CITY = "Udine";
	private static final String BIRTH_DATE = "29-07-1985";
	private static final String DATE_FORMAT = "dd-MM-yyyy";
	
	private Date birthDate = new Date();
	
	@Before
	public void setUp() throws Exception {
		birthDate = new SimpleDateFormat(DATE_FORMAT).parse(BIRTH_DATE);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}
	
	protected Person getValidPerson() {
		Address address = Address.builder() //
								.street(STREET) //
								.city(CITY) //
								.build();
		
		Person person = Person.builder() //
				.name(NAME) //
				.surname(SURNAME) //
				.age(AGE) //
				.address(address) //
				.birthDate(birthDate) //
				.phoneNumber(null) //
				.build();
		return person;
	}
	
}
