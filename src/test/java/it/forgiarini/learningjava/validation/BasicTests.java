package it.forgiarini.learningjava.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;

import it.forgiarini.learningjava.validation.beans.Address;
import it.forgiarini.learningjava.validation.beans.Person;
import it.forgiarini.learningjava.validation.beans.PersonNotAnnotated;

/**
 *	Some learning tests on Java Bean Validation feature (JSR 303)
 */
public class BasicTests extends BaseValidationTest {

	@Test
	public void alwaysValidateNotAnnotatedBean() {
		// given
		PersonNotAnnotated person = PersonNotAnnotated.builder().build();
		
		// when
		Set<ConstraintViolation<PersonNotAnnotated>> violations = validator.validate(person);
		
		// then
		assertEquals(0, violations.size());
	}
	
	@Test
	public void shouldValidatePersonWithAllFieldsSetToValue() {
		// given
		Person person = getValidPerson();
		
		// when
		Set<ConstraintViolation<Person>> violations = validator.validate(person);
		
		// then
		assertEquals(0, violations.size());
	}
	
	@Test
	public void shouldNotValidatePersonWithNullAddress() {
		// given
		Person person = getValidPerson();
		person.setAddress(null);
		
		// when
		Set<ConstraintViolation<Person>> violations = validator.validate(person);
		
		// then
		assertEquals(1, violations.size());
		if (violations.size() == 1) {
			ConstraintViolation<Person> violation = violations.iterator().next();
			Object leafBean = violation.getLeafBean();
			assertSame(leafBean, person);
		}
	}
	
	@Test
	public void testMessageAnnotationOption() {
		// given
		Person person = getValidPerson();
		person.setAddress(null);
		
		// when
		Set<ConstraintViolation<Person>> violations = validator.validate(person);
		
		// then
		if (violations.size() == 1) {
			ConstraintViolation<Person> violation = violations.iterator().next();
			assertEquals(Person.ERROR_MSG_NULL_ADDRESS, violation.getMessage());
		}
	}
	
	@Test
	public void testValidationPropagationOnInnerFields() {
		// given
		Person person = getValidPerson();
		person.getAddress().setCity(null); // To have an invalid Address
		
		// when
		Set<ConstraintViolation<Person>> violations = validator.validate(person);
		
		// then
		assertEquals(1, violations.size());
		if (violations.size() == 1) {
			ConstraintViolation<Person> violation = violations.iterator().next();
			assertSame(violation.getRootBean(), person);
			assertSame(violation.getLeafBean(), person.getAddress());
		}
	}
	
	@Test
	public void constraintViolationSetContainsAllViolationsIfMoreViolationsAreDetected() {
		// given
		Person person = getValidPerson();
		person.setName(null); // To have an invalid Name
		person.setAge(Person.MAX_VALID_AGE + 1); // To have an invalid Age
		person.getAddress().setCity(null); // To have an invalid Address
		
		// when
		Set<ConstraintViolation<Person>> violations = validator.validate(person);
		
		// then
		assertEquals(3, violations.size());
	}
	
	// TODO: test some validation annotations
	
	// TODO: test validation groups
	
	// TODO: custom validators
	
}
