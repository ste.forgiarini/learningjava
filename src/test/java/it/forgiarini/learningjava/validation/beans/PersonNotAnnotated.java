package it.forgiarini.learningjava.validation.beans;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class PersonNotAnnotated {

	private String name;
	private String surname;
	private int age;
	private Date birthDate;
	private Address address;
	
}
