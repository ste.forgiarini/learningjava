package it.forgiarini.learningjava.validation.beans;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class Address {

	private @NotNull @Size(min = 2, max = 50) String street;
	private @NotNull String city;
	private String country;
	private String zipCode;
	
}
