package it.forgiarini.learningjava.validation.beans;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class Person {
	
	public static final String ERROR_MSG_NULL_ADDRESS = "Person address cannot be null";
	public static final int MIN_VALID_AGE = 0;
	public static final int MAX_VALID_AGE = 120;

	private @NotNull String name;
	private @NotNull String surname;
	private @NotNull @Min(MIN_VALID_AGE) @Max(MAX_VALID_AGE) int age;
	private @NotNull Date birthDate;
	private @NotNull(message = ERROR_MSG_NULL_ADDRESS) @Valid Address address;
	private String phoneNumber;
	
}
