package it.forgiarini.learningjava.guava.collections.set;

import java.util.Set;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

public class MiscellaneousTest {

	private Set<Long> initialSet;
	
	@Before
	public void setUp() throws Exception {
		initialSet = Sets.newHashSet(1L, 2L, 3L, 4L, 5L, 6L);
	}

	@Test
	public void intersectionBetweenIdenticalSetsShouldReturnSetViewWithInitialSetSize() {
		// given
		Set<Long> secondSet = Sets.newHashSet(initialSet);
		
		// when
		SetView<Long> resultSetView = Sets.intersection(initialSet, secondSet);
		
		//then
		assertEquals(resultSetView, initialSet);
	}
	
	@Test
	public void intersectionWithEmptySetShouldReturnEmptySet() {
		// given
		Set<Long> emptySet = Sets.newHashSet();
		
		// when
		SetView<Long> resultSetView = Sets.intersection(initialSet, emptySet);
		
		//then
		assertEquals(0, resultSetView.size());
	}
	
	@Test(expected=NullPointerException.class)
	public void intersectionWithNullShouldThrowNullPointerException() {
		// when
		Sets.intersection(initialSet, null);
	}
	
	@Test
	public void testDifferenceBetweenTwoNonEmptySetsIsNotSymmetric() {
		// given
		Set<Long> secondSet = Sets.newHashSet(5L, 6L, 7L, 8L);
		
		// when
		SetView<Long> resultSetView1 = Sets.difference(initialSet, secondSet);
		SetView<Long> resultSetView2 = Sets.difference(secondSet, initialSet);
		
		// then
		assertEquals(resultSetView1, Sets.newHashSet(1L, 2L, 3L, 4L));
		assertEquals(resultSetView2, Sets.newHashSet(7L, 8L));
	}

}
