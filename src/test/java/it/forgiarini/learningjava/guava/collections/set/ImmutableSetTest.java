package it.forgiarini.learningjava.guava.collections.set;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ImmutableSetTest {

	public static ImmutableSet<String> immutableSet;
	
	@Before
	public void setUp() {
		immutableSet = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowUnsupportedExceptionIfTryToAddElements() {
		immutableSet.add("e");
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowUnsupportedExceptionIfTryToRemoveElements() {
		immutableSet.removeIf(Predicates.alwaysTrue());
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void shouldThrowUnsupportedExceptionIfTryClear() {
		immutableSet.clear();
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void shouldCreateAListThatIsImmutable() {
		// given
		List<String> immutableList = immutableSet.asList();
		
		// when
		immutableList.add("z");
	}
	
	@Test
	public void shouldCreateAnImmutableListWithSameElements() {
		// given
		List<String> immutableList = immutableSet.asList();
		
		// when
		Assert.assertTrue(immutableSet.size() == immutableList.size());
		for (String listElement : immutableList) {
			Assert.assertTrue(immutableSet.contains(listElement));
		}
	}
	
	@Test
	public void shouldManipulate() {
		String[] initArray = new String[] {"a", "b", "c"};
		Set<String> set = Sets.newConcurrentHashSet(Lists.newArrayList(initArray));
		System.out.println(set);
		set.forEach(new Consumer<String>() {
			
			public void accept(String t) {
				t = t + "-----";
			}
		});
		System.out.println(set);
	}
	
}
