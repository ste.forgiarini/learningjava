package it.forgiarini.learningjava.guava.collections.filtering;

import static com.google.common.base.Predicates.alwaysFalse;
import static com.google.common.base.Predicates.alwaysTrue;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.instanceOf;
import static com.google.common.base.Predicates.isNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.base.Predicates.or;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import it.forgiarini.learningjava.guava.predicates.CustomPredicates;


public class PredicatesFilteringTest {

	private Set<Long> initialSet;
	
	@Before
	public void setUp() {
		initialSet = Sets.newHashSet();
		for (long l = 0; l < 1000; l++) {
			initialSet.add(l);
		}
	}
	
	@Test
	public void filterMethodReturnsNewFilteredCollectionAndIsNotInPlace() {
		Set<Long> resultSet = Sets.filter(initialSet, alwaysTrue());
		
		assertFalse(initialSet == resultSet);
	}
	
	@Test
	public void alwaysTruePredicateShouldReturnNewUnfilteredSet() {
		Set<Long> resultSet = Sets.filter(initialSet, alwaysTrue());
		
		assertEquals(initialSet, resultSet);
	}
	
	@Test
	public void alwaysFalsePredicateShouldReturnsNewEmptySet() {
		Set<Long> resultSet = Sets.filter(initialSet, alwaysFalse());
		
		assertEquals(0, resultSet.size());
	}
	
	@Test
	public void notNullPredicateShouldReturnSetWithoudNullValues() {
		// given
		int initialSetSize = initialSet.size();
		initialSet.add(null);
		
		// when
		Set<Long> filteredSet = Sets.filter(initialSet, notNull());
		
		// then
		assertEquals(initialSetSize, filteredSet.size());
		assertFalse(filteredSet.contains(null));
	}
	
	@Test
	public void nullPredicateShouldReturnSetWithOnlyNullValues() {
		// given
		initialSet.add(null);
		initialSet.add(null);
		initialSet.add(null);
		
		// when
		Set<Long> filteredSet = Sets.filter(initialSet, isNull());
		
		// then
		assertEquals(1, filteredSet.size());
		assertTrue(filteredSet.contains(null));
	}
	
	@Test
	public void instanceOfPredicateShouldFilterOnlyNotNullElementsOfThatInstanceIncludingSubtypes() {
		// given
		int initialSize = initialSet.size();
		initialSet.add(null);
		
		// when
		Set<Long> onlyLongSet = Sets.filter(initialSet, instanceOf(Long.class));
		
		// then
		assertEquals(initialSize + 1, initialSet.size());
		assertEquals(initialSize, onlyLongSet.size());
		
		// and when 
		Set<Long> onlyNumberSet = Sets.filter(initialSet, instanceOf(Number.class));
		
		// then
		assertEquals(initialSize, onlyNumberSet.size());
		
		// but when 
		Set<Long> onlyStringSet = Sets.filter(initialSet, instanceOf(String.class));
		
		// then
		assertEquals(0, onlyStringSet.size());
	}
	
	@Test
	public void inPredicateShouldFilterElementsContainedInCollectionPassedAsParameter() {
		
		// when
		Set<Long> filteredSet = Sets.filter(initialSet, in(Sets.newHashSet(1L, 2L, -10L)));
		
		// then
		assertTrue(filteredSet.contains(1L));
		assertTrue(filteredSet.contains(2L));
	}
	
	@Test
	public void testPredicatesCompositionWithAndPredicate() {
		
		// when
		Set<Long> filteredSet1 = Sets.filter(initialSet, and(in(Sets.newHashSet(1L, 2L, -10L)), alwaysFalse()));
		Set<Long> filteredSet2 = Sets.filter(initialSet, and(in(Sets.newHashSet(1L, 2L, -10L)), alwaysTrue()));
		Set<Long> filteredSet3 = Sets.filter(initialSet, and(in(Sets.newHashSet(1L, 2L, -10L)), in(Lists.newArrayList(-10L, -100L))));
		
		// then
		assertTrue(filteredSet1.isEmpty());
		assertEquals(2, filteredSet2.size());
		assertTrue(filteredSet3.isEmpty());
	}
	
	@Test
	public void testPredicatesCompositionWithOrPredicate() {
		
		// when
		Set<Long> filteredSet1 = Sets.filter(initialSet, or(in(Sets.newHashSet(1L, 2L, -10L)), alwaysFalse()));
		Set<Long> filteredSet2 = Sets.filter(initialSet, or(in(Sets.newHashSet(1L, 2L, -10L)), alwaysTrue()));
		Set<Long> filteredSet3 = Sets.filter(initialSet, or(in(Sets.newHashSet(1L, 2L, -10L)), in(Lists.newArrayList(3L, 4L))));
		
		// then
		assertEquals(2, filteredSet1.size());
		assertEquals(initialSet.size(), filteredSet2.size());
		assertEquals(4, filteredSet3.size());
	}
	
	@Test
	public void testPredicatesCompositionWithNotPredicate() {
		
		// when
		Set<Long> filteredSet1 = Sets.filter(initialSet, not(in(Sets.newHashSet(1L, 2L, -10L))));
		Set<Long> filteredSet2 = Sets.filter(initialSet, not(in(Sets.newHashSet())));
		
		// then
		assertEquals(initialSet.size() - 2, filteredSet1.size());
		assertEquals(initialSet.size(), filteredSet2.size());
	}
	
	@Test
	public void customPredicateShouldFilterEvenNumbers() {
		
		// given
		Predicate<Number> evenNumbers = CustomPredicates.evenNumbers();
		
		// when
		Set<Long> setOfEvenNumbers = Sets.filter(initialSet, evenNumbers);
		
		// then
		assertEquals(initialSet.size() / 2, setOfEvenNumbers.size());
		for (Long l : setOfEvenNumbers) {
			if (l % 2 != 0) {
				fail();
			}
		}
	}
	
	@Test
	public void customPredicateShouldFilterOddNumbers() {
		
		// given
		Predicate<Number> oddNumbers = CustomPredicates.oddNumbers();
		
		// when
		Set<Long> setOfOddNumbers = Sets.filter(initialSet, oddNumbers);
		
		// then
		assertEquals(initialSet.size() / 2, setOfOddNumbers.size());
		for (Long l : setOfOddNumbers) {
			if (l % 2 == 0) {
				fail();
			}
		}
	}
	
}
