package it.forgiarini.learningjava.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import it.forgiarini.learningjava.spring.config.SpringJavaConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringJavaConfig.class)
public class SpringContextTest {

	private @Autowired MediaPlayer mediaPlayer;
	private @Autowired ApplicationContext ctx;
	private @Autowired List<MediaPlayer> allMediaPlayers;
	
	@Test
	public void onlyToTestIfSpringContainerStarts() {
		assertTrue(true);
	}
	
	@Test
	public void springChooseThePrimaryAnnotatedBean() {
		assertTrue(mediaPlayer instanceof DVDPlayer);
	}
	
	@Test
	public void defaultBeanNameIsTheNameOfTheMethodInJavaConfig() {
		assertSame(mediaPlayer, ctx.getBean("dvdPlayer"));
	}
	
	@Test
	public void beansAreSingleton() {
		// given
		MediaPlayer dvd1 = ctx.getBean("dvdPlayer", MediaPlayer.class);
		MediaPlayer dvd2 = ctx.getBean("dvdPlayer", MediaPlayer.class);
		MediaPlayer dvd3 = ctx.getBean("dvdPlayer", MediaPlayer.class);
		
		// then
		assertSame(mediaPlayer, dvd1);
		assertSame(dvd1, dvd2);
		assertSame(dvd2, dvd3);
		
		// but when
		MediaPlayer cd1 = ctx.getBean("cdPlayer1", MediaPlayer.class);
		assertNotSame(cd1, dvd1);
		assertNotEquals(cd1, dvd1);
	}
	
	@Test
	public void defaultBeanNameIsTheJavaConfigMethodName() {
		assertEquals(2, allMediaPlayers.size());
		for (int i = 0; i < 2; i++) {
			assertTrue(mediaPlayer instanceof DVDPlayer || mediaPlayer instanceof CDPlayer);
		}
	}

}
