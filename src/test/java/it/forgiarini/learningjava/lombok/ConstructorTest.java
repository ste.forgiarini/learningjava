package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class ConstructorTest {

	private static final String name = "Mario";
	private static final String surname = "Rossi";
	private static final Integer age = 30;
	private static final String address = "Via Roma 20";
	
	
	/**
	 * Creates a constructor with no parameter. If the force attribute is set to true, each non initialized field
	 * of the class is automatically initialized to null/0/false
	 */
	@NoArgsConstructor(force = true)
	
	/**
	 * Creates a constructor with n-parameters, where n is the number of the class fields
	 */
	@AllArgsConstructor
	
	/**
	 * Creates a constructor with n-parameters, where n is the number of the class non initialized final fields 
	 * and the fields that are annotated with @NonNull annotation
	 */
	@RequiredArgsConstructor
	@Getter
	private static class Person {
		private String name = null;
		private final String surname;
		private final Integer age;
		@NonNull
		private String address;
	}
	
	@Test
	public void noArgsConstructorShouldCreateNewPersonWithFinalFieldsInitialized() {
		// when
		Person person = new Person();
		
		// then
		assertNull(person.getName());
		assertNull(person.getSurname());
		assertNull(person.getAge());
		assertNull(person.getAddress());
	}
	
	@Test
	public void allArgsConstructorShouldCreateNewPersonWithPassedParameters() {
		
		// when
		Person person = new Person(name, surname, age, address);
		
		// then
		assertEquals(name, person.getName());
		assertEquals(surname, person.getSurname());
		assertEquals(age, person.getAge());
		assertEquals(address, person.getAddress());
	}
	
	@Test
	public void requiredArgsConstructorShouldCreateNewPersonWithPassedParameters() {
		
		// when
		Person person = new Person(surname, age, address);
		
		// then
		assertEquals(surname, person.getSurname());
		assertEquals(age, person.getAge());
		assertEquals(address, person.getAddress());
	}

}
