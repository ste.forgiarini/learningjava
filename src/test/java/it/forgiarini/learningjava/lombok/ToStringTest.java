package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.*;

import org.junit.Test;

public class ToStringTest {

	@Test
	public void testCatToString() {
		
		// given
		Cat cat = new Cat("Toast", 3);
		
		// when
		String humanReadableCat = cat.toString();
		
		// then
		assertTrue(humanReadableCat.matches("Cat.*NAME.*=.*AGE.*=.*"));
	}

}
