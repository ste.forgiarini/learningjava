package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

public class EqualsTest {

	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	@AllArgsConstructor
	private static class Person {
		@EqualsAndHashCode.Include
		private String name;
		@EqualsAndHashCode.Include
		private String surname;
		private Integer age;
		@EqualsAndHashCode.Include
		private Cat cat;
	}
	
	@EqualsAndHashCode(exclude = {"density", "population"})
	@AllArgsConstructor
	private static class City {
		private String name;
		private String zipCode;
		private int population;
		private double density;
	}
	
	@Test
	public void instancesShouldBeEqualsIfOnlyIncludedFieldsAreEquals() {
		
		// given
		Person person = new Person("Mario", "Rossi", 30, new Cat("Toast", 3));
		Person person2 = new Person("Mario", "Rossi", 45, new Cat("Toast", 3));
		
		// then
		assertEquals(person, person2);
		
		// but when
		Person person3 = new Person("Stefano", "Rossi", 30, new Cat("Toast", 3));
		
		// then
		assertNotEquals(person3, person);
		assertNotEquals(person3, person2);
	}
	
	@Test
	public void instancesShouldBeEqualsIfAllFieldsExceptExcludedFieldsAreEquals() {
		
		// given
		City city = new City("Udine", "33100", 100_000, 123.2);
		City city2 = new City("Udine", "33100", 150_000, 333.2);
		City city3 = new City("Udine", "44444", 100_000, 123.2);
		
		// then
		assertEquals(city, city2);
		assertNotEquals(city, city3);
	}
	
	@Test
	public void testAllFieldsConsidered() {
		
		// given
		Cat cat = new Cat("Toast", 3);
		Cat cat2 = new Cat("Toast", 3);
		Cat cat3 = new Cat("Toast", 5);
		Cat cat4 = new Cat("Molly", 5);
		
		// then
		assertEquals(cat, cat2);
		assertNotEquals(cat, cat3);
		assertNotEquals(cat, cat4);
		assertNotEquals(cat3, cat4);
	}

}
