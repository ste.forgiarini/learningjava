package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public class GetterAndSetterTest {

	@Getter
	@Setter
	private static class Person {
		
		private String name;
		private String surname;
		private String address;
		private Integer age;
		@Setter(AccessLevel.NONE)  // Overrides the annotation declared at type level. The get method won't be generated
		private String cf;
		private static final String country = "Italy"; // Getter and setter will not be generated for static fields
		
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void setGeneratedMethodsShouldCorrectlySetCorrespondingFields() {
		
		// given
		Person person = new Person();
		String name = "Mario";
		String surname = "Rossi";
		String address = "Via Roma 20";
		Integer age = 30;
		
		// when
		person.setName(name);
		person.setSurname(surname);
		person.setAddress(address);
		person.setAge(age);
		
		// then
		assertEquals(name, person.getName());
		assertEquals(surname, person.getSurname());
		assertEquals(address, person.getAddress());
		assertEquals(age, person.getAge());
	}

}
