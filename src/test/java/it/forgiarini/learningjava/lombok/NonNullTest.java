package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

public class NonNullTest {
	
	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	private static class Animal {
		
		@NonNull
		private String name;
		private Integer age;
		
		public void addYears(@NonNull Integer yearsToAdd) {
			age = age + yearsToAdd;
		}
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@Test(expected=NullPointerException.class)
	public void shouldThrowExceptionIfNullIsPassedToConstructorForNonNullAnnotatedField() {
		
		// when
		try {
			Animal animal = new Animal("Toast", 3);
		} catch (Exception ex) {
			fail();
		}
		
		// but when
		Animal animal2 = new Animal(null, 3);
	}
	
	@Test(expected=NullPointerException.class)
	public void shouldThrowExceptionIfNullIsPassedToSetterMethodForNonNullAnnotatedField() {
		// given
		Animal animal = new Animal();
		
		// when 
		try {
			animal.setAge(null);
		} catch(Exception ex) {
			fail();
		}
		
		// but when
		animal.setName(null);
	}
	
	@Test(expected=NullPointerException.class)
	public void shouldThrowExceptionIfNullIsPassedAsParameterToNonNullAnnotatedParameter() {
		
		// given
		Animal animal = new Animal("Toast", 3);
		
		// when
		animal.addYears(null);
	}

}
