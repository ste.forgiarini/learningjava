package it.forgiarini.learningjava.lombok;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

public class BuilderTest {

	private static final String NAME = "Mario";
	private static final String SURNAME = "Rossi";
	private static final int AGE = 40;
	private static final String ADDRESS = "Via Roma 20";
	
	@Builder(toBuilder = true)
	@Getter
	@EqualsAndHashCode
	private static class Person {
		
		private final String name;
		private final String surname;
		private int age;
		private String address;
	}
	
	@Test	
	public void testBuilderAnnotation() {
		
		// given
		Person person = Person.builder().age(AGE)
								.name(NAME)
								.surname(SURNAME)
								.address(ADDRESS)
								.build();
		
		// then
		assertEquals(NAME, person.getName());
		assertEquals(SURNAME, person.getSurname());
		assertEquals(AGE, person.getAge());
		assertEquals(ADDRESS, person.getAddress());
	}
	
	@Test
	public void toBuilderMethodCreatesNewBuilder() {
		// given
		Person.PersonBuilder personBuilder = Person.builder();
		Person person = personBuilder.age(AGE)
								.name(NAME)
								.surname(SURNAME)
								.address(ADDRESS)
								.build();
		
		// when
		Person.PersonBuilder clonedPersonBuilder = person.toBuilder();
		
		// then
		assertNotSame(clonedPersonBuilder.build(), personBuilder.build());
		assertEquals(clonedPersonBuilder.build(), personBuilder.build());
	}
	
	@Test
	public void toBuilderOptionCreatesBuilderThatWillBuildAnotherInstanceWithSameFieldValues() {
		// given
		Person person = Person.builder().age(AGE)
				.name(NAME)
				.surname(SURNAME)
				.address(ADDRESS)
				.build();
		
		// when
		Person clonedPerson = person.toBuilder().build();
		
		// then
		assertTrue(person != clonedPerson);
		assertEquals(person, clonedPerson);
		
		// but when
		Person anotherClonedPerson = clonedPerson.toBuilder().age(AGE + 1).build();
		
		// then
		assertTrue(person != anotherClonedPerson);
		assertTrue(clonedPerson != anotherClonedPerson);
		assertNotEquals(person, anotherClonedPerson);
		assertNotEquals(clonedPerson, anotherClonedPerson);
	}

}
