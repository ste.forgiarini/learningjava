package it.forgiarini.learningjava.javastd.basic;

import org.junit.Assert;
import org.junit.Test;

public class StringTest {

	@Test
	public void stringsAreImmutable() {
		// given
		String s = "Rome";
		
		// when
		s.replace("e", "a");
		
		// then
		Assert.assertEquals("Rome", s);
		
		// but when
		String s2 = s.replace("e", "a");
		
		// then
		Assert.assertEquals("Roma", s2);
	}
	
}
