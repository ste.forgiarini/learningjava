package it.forgiarini.learningjava.javastd.basic;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class StreamTest {

	private static class Person {

		private String name;
		private int age;
		
		public Person(String name, int age) {
			this.name = name;
			this.age = age;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

	}

	@Test
	public void reduce_standardUse() {
		// given
		List<String> letters = Arrays.asList("A", "B", "C", "D", "E");

		// when
		String reduced = letters.stream().reduce("0", (a, b) -> a + "_" + b);

		// then
		assertEquals("0_A_B_C_D_E", reduced);
	}

	@Test
	public void reduce_ifOneElementReturnApplyFunctioneOnceIfIdentitySpecified() {
		// given
		List<String> letters = Arrays.asList("A");

		// when
		String reduced = letters.stream().reduce("ZZ", (a, b) -> a + b + "_");

		// then
		assertEquals("ZZA_", reduced);
	}

	@Test
	public void reduce_returnIdentityParameterIfEmptyStream() {
		// given
		List<String> letters = Arrays.asList();

		// when
		String reduced = letters.stream().reduce("ZZ", (a, b) -> a + b + "_");

		// then
		assertEquals("ZZ", reduced);
	}

	@Test
	public void reduce_returnTheLastElement() {
		// given
		List<String> letters = Arrays.asList("A", "B", "C", "D");

		// when
		String reduced = letters.stream().reduce(this::chooseSecondLetter).orElse("");

		// then
		assertEquals("D", reduced);
	}

	private String chooseSecondLetter(String a, String b) {
		return b;
	}

	/**
	 * Peek is like forEach. The difference is that it is used to implement an
	 * intermediate operation instead of an end operation. An intermediate operation
	 * returns a stream whereas an end operation returns void. The peek method is
	 * used to perform an intermediate operation and then let the stream continue
	 * the pipeline. An end operation is the last operation performed on the
	 * pipeline
	 */
	@Test
	public void peek_isIntermediateOperation() {
		// given
		Person person1 = new Person("Stefano", 10);
		Person person2 = new Person("Maria", 20);
		List<Person> persons = Arrays.asList(person1, person2);

		// when
		Person result = persons.stream()
				.peek(p -> p.setAge(p.getAge() + 20))
				.filter(p -> p.getAge() > 35)
				.reduce((a, b) -> {throw new RuntimeException("More than one result. Expected one.");})
				.orElseThrow(() -> new RuntimeException("No result found. Expected one."));

		// then
		assertEquals("Maria", result.getName());
	}

}
