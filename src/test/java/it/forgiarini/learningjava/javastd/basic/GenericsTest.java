package it.forgiarini.learningjava.javastd.basic;

import static org.junit.Assert.*;

import org.junit.Test;

public class GenericsTest {

	@Test(expected = ArrayStoreException.class)
	public void typeCheckInArraysIsPerformedRuntime() {
		// given
		Object[] objects = new Long[1];
		
		// when
		objects[0] = 1L; // it's ok
		
		// but when
		objects[0] = "string"; // then throws exception 
	}
	
}
