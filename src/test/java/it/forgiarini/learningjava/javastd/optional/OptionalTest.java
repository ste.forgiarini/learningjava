package it.forgiarini.learningjava.javastd.optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.Test;


public class OptionalTest {

	@Test(expected = NullPointerException.class)
	public void of_exceptionIfNullIsPassedAsParameter() {
		// given
		String s = null;
		
		// when
		Optional.of(s);
	}
	
	@Test
	public void ofNullable_createsNewOptionalObjectAndNoExceptionAreThrown() {
		// given
		String s1 = null;
		String s2 = "not_empty";
		
		// when
		Optional<String> opt1 = Optional.ofNullable(s1);
		Optional<String> opt2 = Optional.ofNullable(s2);
		
	}
	
	@Test(expected = NoSuchElementException.class)
	public void get_exceptionIfNotPresent() {
		// given
		Optional<String> opt = Optional.empty();
		
		// when
		String inner = opt.get();
		
		// then
		assertNull(inner);
	}
	
	@Test
	public void isPresent_falseIfNotPresent() {
		// given
		Optional<String> opt = Optional.empty();
		
		// when
		boolean isPresent = opt.isPresent();
		
		// then
		assertFalse(isPresent);
	}
	
	@Test
	public void isPresent_trueIfPresent() {
		// given
		Optional<String> opt = Optional.of("");
		
		// when
		boolean isPresent = opt.isPresent();
		
		// then
		assertTrue(isPresent);
	}
	
	@Test
	public void orElse_returnsPassedParameterOnlyIfNotPresent() {
		// given
		Optional<String> opt = Optional.empty();
		Optional<String> opt2 = Optional.of("not_empty");
		String alternative = "alternative";
		
		// when
		String actual = opt.orElse(alternative);
		String actual2 = opt2.orElse(alternative);
		
		// then
		assertEquals("alternative", actual);
		assertEquals("not_empty", actual2);
	}

}
