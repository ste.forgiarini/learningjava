package it.forgiarini.learningjava.javastd.collections.utils;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


public class CollectionsTest {
	
	private List<String> initialList;
	private Set<String> initialSet;

	@Before
	public void setUp() throws Exception {
		initialList = Lists.newArrayList("A", "B", "C", "C", "D");
		initialSet = Sets.newHashSet(initialList);
	}

	@Test
	public void frequencyMethodShouldReturnTheNumberOfOccurencesOfAnElementInACollection() {
		
		// when
		int frequency0 = Collections.frequency(initialList, "Z");
		int frequency1 = Collections.frequency(initialList, "A");
		int frequency2 = Collections.frequency(initialList, "C");
		int frequencyNull = Collections.frequency(initialList, null);
		
		// then
		assertEquals(0, frequency0);
		assertEquals(1, frequency1);
		assertEquals(2, frequency2);
		assertEquals(0, frequencyNull);
		
		// but given
		initialList.add(null);

		// when
		frequencyNull = Collections.frequency(initialList, null);
		
		// then
		assertEquals(1, frequencyNull);
	}

}
