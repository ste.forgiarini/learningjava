package it.forgiarini.learningjava.javastd.collections.set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Sets;


public class SetTest {

	/**
	 * The HashSet NOT ensure that the iterator always returns an element such that is greather/equal to the previous one
	 */
	@Test
	public void hashSetIsNotOrdered() {
		// given
		Set<Double> hashSet = new HashSet<>();
		for (int i = 0; i < 200; i++) {
			hashSet.add(Math.random() * 200);
		}
		
		// when
		boolean isOrdered = isSetOrdered(hashSet);
		
		// then 
		Assert.assertFalse(isOrdered);
	}
	
	/**
	 * The TreeSet ensures that the iterator always returns an element that is greather(or equals) to the previous one
	 */
	@Test
	public void treeSetIsOrdered() {
		// given
		Set<Double> treeSet = new TreeSet<>();
		for (int i = 0; i < 200; i++) {
			treeSet.add(Math.random() * 200);
		}
		
		// when
		boolean isOrdered = isSetOrdered(treeSet);
		
		// then 
		Assert.assertTrue(isOrdered);
	}
	
	/**
	 * A Set cannot contain two elements E1 and E2 such that E1.equals(E2)
	 * For each element E in Set, there is only one element equals to E (E itself)
	 */
	@Test
	public void setsCannotContainDuplicates() {
		// given
		String[] initArray = new String[] {"a", "b", "c"};
		Set<String> actualSet = Sets.newHashSet(initArray);
		Set<String> expectedSet = Sets.newHashSet(initArray);
		
		// when
		actualSet.add("a");
		actualSet.add("c");
		
		// then
		Assert.assertTrue(actualSet.size() == 3);
		Assert.assertTrue(actualSet.containsAll(expectedSet));
		Assert.assertTrue(expectedSet.containsAll(actualSet));
	}
	
	private boolean isSetOrdered(Set<Double> set) {
		boolean ordered = true;
		Double lastProcessed = -1D;
		for (Double currentProcessed : set) {
			boolean currentProcessedIsLowerEquals = currentProcessed.compareTo(lastProcessed) < 1;
			if (currentProcessedIsLowerEquals) {
				ordered = false;
				break;
			}
			lastProcessed = currentProcessed;
		}
		return ordered;
	}
	
}
